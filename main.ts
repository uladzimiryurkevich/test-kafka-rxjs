import {interval} from "rxjs";
import {bufferCount} from "rxjs/operators";

import {Kafka} from "kafkajs";
import {kafkaProduce} from "./operators/kafkaProduce";
import {kafkaConsume} from "./operators/kafkaConsume";

// Research by Vladimir Yurkevich
// https://netbasal.com/creating-custom-operators-in-rxjs-32f052d69457
// https://fireship.io/lessons/custom-rxjs-operators-by-example/

const kafka = new Kafka({
    clientId: 'my-app2',
    brokers: ['localhost:9092'],
    //logLevel: logLevel.ERROR
});
const topic = 'initial';


const consumeObs = kafkaConsume(kafka, 'my-group6', topic)
    .pipe(bufferCount(2))
    .subscribe(messages => messages.forEach(message => console.log(message.value.toString())));

const produceObs = interval(1000)
    .pipe(
        kafkaProduce(kafka, topic)
    )
    .subscribe();


function exitHandler(options, exitCode) {
    if (options.cleanup) {
        console.log('Cleaning...');
        consumeObs.unsubscribe();
        produceObs.unsubscribe();
    }
    if (exitCode || exitCode === 0) console.log(exitCode);
    if (options.exit) process.exit();
}

//do something when app is closing
process.on('exit', exitHandler.bind(null, {cleanup: true}));

//catches ctrl+c event
process.on('SIGINT', exitHandler.bind(null, {exit: true}));

// catches "kill pid" (for example: nodemon restart)
process.on('SIGUSR1', exitHandler.bind(null, {exit: true}));
process.on('SIGUSR2', exitHandler.bind(null, {exit: true}));

//catches uncaught exceptions
process.on('uncaughtException', exitHandler.bind(null, {exit: true}));

