import {Kafka, KafkaMessage} from "kafkajs";
import {Observable} from "rxjs";

export function kafkaConsume(kafka: Kafka, groupId: string, topic: string) {
    return new Observable<KafkaMessage>(subscriber => {
        const consumer = kafka.consumer({groupId});

        consumer.connect()
            .then(() =>
                consumer.subscribe({topic}))
            .then(() =>
                consumer.run({
                    // autoCommitInterval: 1000,
                    eachMessage: async ({topic, partition, message}) => subscriber.next(message),
                }))
            .catch(err => subscriber.error(err));

        return async () => {
            await consumer.disconnect();
        };
    });
}