import {Kafka} from "kafkajs";
import {Observable} from "rxjs";
import {v4 as uuidv4} from 'uuid';

export function kafkaProduce(kafka: Kafka, topic: string) {
    return function <T>(source: Observable<T>): Observable<T> {
        return new Observable(subscriber => {
            const producer = kafka.producer();
            let subscription;

            producer.connect().then(() => {
                subscription = source.subscribe({
                    next(value) {
                        if (value !== undefined && value !== null) {
                            subscriber.next(value);
                        }
                        producer.send({
                            topic,
                            messages: [
                                {key: uuidv4(), value: value.toString()},
                            ],
                        }).catch(subscriber.error);
                    },
                    error(error) {
                        subscriber.error(error);
                    },
                    complete() {
                        subscriber.complete();
                    }
                });
            }).catch(subscriber.error);

            return async () => {
                subscription.unsubscribe();
                await producer.disconnect();
            }
        });
    }
}